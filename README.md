# ChipFlow GFMPW template

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

This is template repository used as the base for GFMPW tapeout for ChipFlow projects.
It contains reduced setup based on the `gf180mcu` branch in the [caravel_user_project repo](https://github.com/efabless/caravel_user_project).
The commit is squashed to save disk space and download bandwidth.

The use of this template is mainly to allow to easily make a repo that can be used to commit
a design for tape-out on the efabless platform. Repo that are based on this template should
put the user_project_wrapper.gds(.gz) to tape out in the gds directory and replace this
README with a proper reference to the repo and code that is used to generate this gds file.

The branch `efabless-caravel` branch is used to track the upstream repo. Updating to new version
is done by manually adding squashed changes in the upstream `gf180mcu` to the commit version
mentioned in the last commit on the `efabless-caravel` branch. In the new commit message on the
`efabless-caravel` branch the new commit version of the upstream repo should be mentioned so
a new update can done in the future. Only squashed changes are added in this branch to avoid
brining in the whole (convoluted) history of the upstream branch.

After an update to the `efabless-caravel` project it needs to be merged into the `main` branch.
This most likely will involve fixing merge conflicts; each merge conflict will need to be
what changes need to be done to `main` branch to reflect the changes in the `efabless-caravel`
branch.
